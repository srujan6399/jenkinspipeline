FROM tomcat:8.0.48-jre8
MAINTAINER srujan.d87@gmail.com
ADD http://mirrors.jenkins.io/war-stable/latest/jenkins.war /usr/local/tomcat/webapps/jenkins.war
EXPOSE 9090
CMD ["catalina.sh", "run"]
