@NonCPS
def jsonParse(def json) {
 new groovy.json.JsonSlurperClassic().parseText(json)
}
pipeline{
    agent any
    stages{
        stage('checkout'){
            steps{
                script{
                    echo 'CHeckout'
                }
            }
        }
        stage('truffleHog'){
            steps{
                script{
                    bat'''
                        truffleHog --json --regex --entropy=false E:\\Bitbucket\\MainWorkflow\\SSH\\ssh
                    '''
                    def vakue = "${BUILD_URL}/consoleText"
                    def response = httpRequest ignoreSslErrors:true, url:vakue
                    def test = response.content
                    println(">>>>>>>>>>>>>>>>>>>>>>>>>>>"+test);
                    if (test.contains("reason")){
                        error 'aborted due to Found some secrets in source code'
                    }
                }
            }
        }
    }
}