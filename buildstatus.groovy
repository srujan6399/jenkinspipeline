def branchname = "${env.branchname}"

pipeline{
    agent any
    stages{
        stage('Message'){
            steps{
                script{
                    echo 'Welcome to DevOps & Bitbucket Cloud!!!!'
                }
            }
        }
        stage('checkout'){
            steps{
                script{
                        git  branch: branchname, credentialsId: '009d48d9-b896-45e8-9f88-28a7d75920b2', url: 'https://srujan6399@bitbucket.org/srujan_devops/test-buildstatus.git'
                }
            }
        }
        // stage('Build'){
        //     steps{
        //         script{
        //             bat 'msbuild calculatordemo\\CalDemo.sln /p:configuration=Release'
        //         }
        //     }
        // }
    }
    post{
        success{
            script{
                bitbucketStatusNotify(
                    buildState: 'SUCCESSFUL',
                    buildKey: 'TES',
                    buildName: 'Test-CodeReview'
                )
            }
            
        }
        failure{
            script{
                bitbucketStatusNotify(
                    buildState: 'FAILED',
                    buildKey: 'TES',
                    buildName: 'Test-CodeReview'
                )
            }
            
        }
    }
}