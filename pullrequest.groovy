pipeline{
    agent any
    stages{
        stage('checkout'){
            steps{
                script{
                    git  branch: 'master', credentialsId: '009d48d9-b896-45e8-9f88-28a7d75920b2', url: 'https://srujan6399@bitbucket.org/devopstx1/demo.git'
                }
            }
        }
        stage('Message'){
            steps{
                script{
                    echo 'Building master branch'
                }
            }
        }
    }
    post{
        success{
            script{
                bitbucketStatusNotify(
                    buildState: 'SUCCESSFUL',
                    buildKey: 'DEM',
                    buildName: 'demoproject'
                )
            }
            
        }
        failure{
            script{
                bitbucketStatusNotify(
                    buildState: 'FAILED',
                    buildKey: 'DEM',
                    buildName: 'demoproject'
                )
            }
            
        }
    }
}